import React from 'react';
import firebase from '@react-native-firebase/app'
import { NavigationContainer } from '@react-navigation/native';
import AppNavigation from './src/navigations';

var firebaseConfig = {
  apiKey: "AIzaSyBdfdVRZ1Bc_QoYiPiu1W3cRjGsR-ioALQ",
  authDomain: "rnsanbercode.firebaseapp.com",
  projectId: "rnsanbercode",
  storageBucket: "rnsanbercode.appspot.com",
  messagingSenderId: "235590101240",
  appId: "1:235590101240:web:292b6b02be58f77c1ff30d",
  measurementId: "G-9ZDJ7YSLYX"
};

if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const App: () => React$Node = () => {
  return (
    <NavigationContainer>
      <AppNavigation />
    </NavigationContainer>
  );
};

export default App;
