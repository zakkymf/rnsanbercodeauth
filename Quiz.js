import React, { useState, useEffect } from 'react'
import { View, Text } from 'react-native'

const Quiz = () => {

    const data = [
        {
            id: 1,
            title: 'Test',
            description: 'Lorem ipsum'
        },
        {
            id: 2,
            title: 'Test 2',
            description: 'Lorem ipsum 1'
        },
        {
            id: 3,
            title: 'Test 3',
            description: 'Lorem ipsum 2'
        }
    ]

    const [name, setName] = useState('Jhon Doe')

    useEffect(() => {
        setTimeout(() => {
            setName('Zakky Muhammad Fajar')
        }, 2000)
        return () => {
            console.log("ini dijalankan")
            setName('Achmad Hilmy')
        }
    }, [])

    const daftarHobi = ['Olahraga', 'Makan', 'Ngoding']
    let hobiFavorit
    for(const hobi of daftarHobi) {
        hobiFavorit = hobi
    }
    // console.log("🚀 ~ file: Quiz.js ~ line 48 ~ Quiz ~ hobiFavorit", hobiFavorit)

    return (
        <View>
            {
                data.map((item) => {
                    return (
                        <Text>{item.title}</Text>
                    )
                })
            }
        </View>
    )
}

export default Quiz
