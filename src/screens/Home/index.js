import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import auth from '@react-native-firebase/auth';

const Home = () => {

    const [user, setUser] = useState({})

    useEffect(() => {
        const userInfo = auth().currentUser
        setUser(userInfo)
    }, [])

    return (
        <View style={styles.container}>
            <Text>{user.email}</Text>
            <Text>{user.uid}</Text>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffffff'
    }
})
