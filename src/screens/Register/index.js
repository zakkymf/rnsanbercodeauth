import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert } from 'react-native'
import auth from '@react-native-firebase/auth';

const Register = () => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const onRegisterPress = () => {
        if(email == '' || password == '') {
            Alert.alert('Error', 'Form tidak boleh kosong')
        } else {
            auth().createUserWithEmailAndPassword(email, password)
            .then((res) => {
                Alert.alert('Sukses', 'Buat akun berhasil')
            })
            .catch((err) => {
                Alert.alert('Error', err.message)
            })
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.formContainer}>
                <TextInput
                    style={styles.input}
                    placeholder="Email"
                    value={email}
                    onChangeText={(text) => setEmail(text)}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    value={password}
                    onChangeText={(text) => setPassword(text)}
                />
                <TouchableOpacity style={styles.btnLogin} onPress={onRegisterPress}>
                    <Text style={styles.btnLoginText}>REGISTER</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#ffffff'
    },
    formContainer: {
        padding: 20,
        flexDirection: 'column'
    },
    input: {
        width: '100%',
        height: 50,
        marginVertical: 5,
        borderColor: '#c6c6c6',
        borderBottomWidth: 1
    },
    btnLogin: {
        height: 50,
        borderRadius: 5,
        marginVertical: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#3a86ff'
    },
    btnLoginText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ffffff'
    }
})
