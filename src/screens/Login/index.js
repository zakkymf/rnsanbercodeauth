import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert } from 'react-native'
import auth from '@react-native-firebase/auth';

const Login = ({ navigation }) => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const onLoginPress = () => {
        if(email == '' || password == '') {
            Alert.alert('Error', 'FOrm tidak boleh kosong')
        } else {
            auth().signInWithEmailAndPassword(email, password)
            .then((res) => {
                navigation.navigate('Home')
            })
            .catch((err) => {
                Alert.alert('Error', err.message)
            })
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.formContainer}>
                <TextInput
                    style={styles.input}
                    placeholder="Email"
                    value={email}
                    onChangeText={(email) => setEmail(email)}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    value={password}
                    secureTextEntry
                    onChangeText={(password) => setPassword(password)}
                />
                <TouchableOpacity style={styles.btnLogin} onPress={onLoginPress}>
                    <Text style={styles.btnLoginText}>LOGIN</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnLogin} onPress={() => navigation.navigate('Register')}>
                    <Text style={styles.btnLoginText}>DAFTAR</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: '#ffffff'
    },
    formContainer: {
        padding: 20,
        flexDirection: 'column'
    },
    input: {
        width: '100%',
        height: 50,
        marginVertical: 5,
        borderColor: '#c6c6c6',
        borderBottomWidth: 1
    },
    btnLogin: {
        height: 50,
        borderRadius: 5,
        marginVertical: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#3a86ff'
    },
    btnLoginText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ffffff'
    }
})
